export const addUsers = (users) => dispatch => {
    dispatch({ type:'ADD_USERS', data: users})
};

export const addPosts = (posts) => dispatch => {
    dispatch({ type:'ADD_POSTS', data: posts})
};