const initialState = []
export default (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_POSTS':
            return action.data.sort((a, b) => a.title.toUpperCase() > b.title.toUpperCase() ? 1 : -1);
        default:
            return state
    }
}