import React, { Component } from 'react'
import { connect } from 'react-redux'
import './App.css'
import { Route, Switch, withRouter } from "react-router-dom"
import request from './lib/request'
import Users from './components/Users'
import Posts from './components/Posts'
import Comments from "./components/Comments"
import Error from "./components/Error"
import {addPosts, addUsers} from "./actions/actions"

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null
        };
    }

    componentDidMount = async () => {
        try {
            const users = await request('https://jsonplaceholder.typicode.com/users');
            const posts = await request('https://jsonplaceholder.typicode.com/posts');
            this.props.addUsers(users);
            this.props.addPosts(posts);
        }
        catch(err) {
            console.error('Couldn\'t get data from server: ' + err);
        }
    }

    componentDidCatch = (error) => {
        console.log('componentDidCatch', error.message);
        this.setState({
            error: error.message
        });
    }

    render() {
        console.log('render', this.state.error)
        if (this.state.error){
            return <Error/>
        }
        return (
            <Switch>
                <Route exact path="/" component={Users}/>
                <Route path="/user/:id" component={Posts}/>
                <Route path="/post/:postId/comments" component={Comments}/>
            </Switch>
        );

    }
}

const mapDispatchToProps = { addUsers, addPosts }

export default withRouter(connect(null, mapDispatchToProps)(App))