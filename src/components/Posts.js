import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router-dom"
import Modal from "./Modal"
import Button from "./Button"
import cx from 'classnames'

class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalPost: null
        }
    }

    openModal = (post) => {
        this.setState({modalPost: post})
    }
    closeModal = () => {
        this.setState({modalPost: null})
    }

    render() {
        //throw new Error("Error from posts");
        return (
            <div>
                <ul className="postsList">
                    {this.props.posts
                        .filter(post => (+post.userId === +this.props.match.params.id))
                        .map(post => (
                            <li key={post.id}>
                                <Link to={`/post/${post.id}/comments`}><h3>{post.title}</h3></Link>
                                <Button
                                    className={cx({
                                        activeBtn: !this.state.modalPost
                                    }, 'btn')}
                                    onClick={() => {
                                        this.openModal(post)}}
                                    text="See full post"/>
                            </li>))}
                </ul>
                <Link to='/'>Back to users</Link>
                {this.state.modalPost && <Modal onCloseClick={this.closeModal} post={this.state.modalPost}/>}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    posts: state.posts,
})

export default connect(mapStateToProps)(Posts)