import React from 'react'
import { connect } from 'react-redux'
import { Link } from "react-router-dom"

const Users = (props) => (
    <ul className="usersList">
        {props.users.map( user =>
            (<li key={user.id}>
                <Link to={`/user/${user.id}`}>
                    {user.name + ' '}
                    ({props.posts.filter( post => post.userId === user.id).length})
                </Link>
            </li>) )}
    </ul>
);

const mapStateToProps = (state) => {
    return ({
        users : state.users,
        posts : state.posts,
    })
}

export default connect(mapStateToProps, null)(Users)