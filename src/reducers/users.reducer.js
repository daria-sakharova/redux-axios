const initialState = []
export default (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_USERS':
            return action.data.sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1);
        default:
            return state
    }
}