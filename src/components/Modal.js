import React, { Component } from 'react'
import Button from "./Button"
import cx from "classnames"

class Modal extends Component {
    constructor(props){
        super(props)
        this.container = null

    }

    handleClick = (e) => {
        const target = e.target;
        if (target !== this.container && target.parentNode !== this.container){
            this.props.onCloseClick();
        }
    };

    render() {
        return (
            <div className="modalWrapper" onClick={this.handleClick}>
                <div ref={ (elem) => {this.container = elem} } className="modalContainer">
                    <h3>{this.props.post.title}</h3>
                    <p>{this.props.post.body}</p>
                    <Button
                        className={cx('btn')}
                        onClick={this.props.onCloseClick}
                        text="Close"/>
                </div>
            </div>
        );
    }
}

export default Modal;