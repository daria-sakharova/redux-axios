import React, { Component } from 'react'

class Error extends Component {
    render() {
        return (
            <div className="modalWrapper">
                <div className="modalContainer">
                    <h3>An error occurred during runtime</h3>
                    <a href="/"><b>Try from the start</b></a>
                </div>
            </div>
        );
    }
}

export default Error;