import React, { Component } from 'react'
import { withRouter } from "react-router-dom"
import request from "../lib/request"
import { Dimmer, Loader } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

class Comments extends Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    componentDidMount = async () => {
        try {
            const comments = await request(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.postId}/comments`);
            this.setState({ comments: comments.slice() }, () => {});
        }
        catch(err) {
            console.error('Couldn\'t get data from server: ' + err);
        }
    }

    handleClick = (e) => {
        e.preventDefault();
        this.props.history.goBack();
    }

    render() {
        if (this.state.comments) {
            return (
                <div>
                    <ul className="postsList">
                        {this.state.comments.map( com => (
                                <li key={'' + com.id}>
                                    <h4>{com.name}</h4>
                                    <p>{com.body}</p>
                                </li>)
                        )}
                    </ul>
                    <a href="#" onClick={this.handleClick}>Back to this user's posts</a>
                </div>
            )
        }
        return (<Dimmer active>
                    <Loader size='massive'>Loading</Loader>
                </Dimmer>
        );
    }
}

export default withRouter(Comments);