import axios from "axios"

async function request(url) {
    const res = await axios.get(url)
    return res.data;
}

export default request;